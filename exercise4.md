## Примеры эндпоинтов

Эндпоинт мы будем записывать следующим образом: `{METHOD} /api/{VERSION}/{CLASS}`. 

- METHOD — GET, POST и т.д.
- VERSION — версия API (в нашем случае будем использовать `v1`)
- CLASS — класс объектов, к которому мы обращаемся. Обычно они соответствуют названиям классов в программировании или коллекций в базе данных. К примеру, если там указано значение `users`, то эндпоинт `GET /api/v1/users`, вероятно, отвечает за _получение_ списка пользователей. Давай рассмотрим другой эндпоинт: `POST /api/v1/collection`. Скорее всего, он отвечает за добавление какой-то коллекции. 

В задании же требуется описать 4 эндпоинта с методами из CRUD (один эндпоинт на каждый метод). Тему приложения ты уже знаешь!

## Эндпоинты

1. **Создание нового рецепта**

 `POST/api/v1/new-year/recipes`

URL запроса: https://www.gastronom.ru/api/v1/new-year/recipes

Тело запроса:
```
{
  "id": 0,
  "name": "slivochnoe-pivo",
  "dueDate": "2023-06-07T10:57:03",
  "completed": true
}
```

2. **Получение полного списка рецептов**

`GET/api/v1/new-year/recipes`

URL запроса: https://www.gastronom.ru/api/v1/new-year/recipes

Тело запроса: отсутствует

3. **Обновлении информации конкретного рецепта, ID{1}**

`PUT/api/v1/new-year/recipes/{1}`

URL запроса: https://www.gastronom.ru/api/v1/new-year/recipes/1

```
{
  "id": 1,
  "name": "zapechennaya-utka",
  "dueDate": "22023-06-07T11:15:35",
  "completed": true
}
```

4. **Удаление рецепта, где 2 - ID рецепта, который необходимо удалить**

`DELETE/api/v1/new-year/recipes/{2}`

URL запроса: https://www.gastronom.ru/api/v1/new-year/recipes/2

Тело запроса: отсутствует

