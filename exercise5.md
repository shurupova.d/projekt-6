## Блоки:
1. Activities (8)
2. Authors (10)
3. Books (8)
4. CoverPhotos (10)
5. Users (8)

Всего: 44

## 1. Activities 

### Успешные:

1.1 GET/api​/v1​/Activities<br/>
HTTP-метод - GET <br/>
Полный URL запроса - <https://fakerestapi.azurewebsites.net/api/v1/Activities> <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует<br/>
Статус-код ответа - 200 <br/>
Тело ответа - 
```
[
  {
    "id": 0,
    "title": "string",
    "dueDate": "2023-06-06T13:37:28.742Z",
    "completed": true
  }
]
```

1.2 POST/api​/v1​/Activities <br/>
HTTP-метод - POST <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Activities <br/>
Заголовки запроса - "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса -

```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-06T21:12:51.288Z",
  "completed": true
}
``` 

Статус-код ответа - 200 <br/>
Тело ответа - 
```
[
  {
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-06T21:12:51.288Z",
  "completed": true
}
]

```

1.3 GET/api​/v1​/Activities​/{id}<br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Activities/1 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - 
```
[
  {
  "id": 1,
  "title": "Activity 1",
  "dueDate": "2023-06-06T22:16:08.9026601+00:00",
  "completed": false
}]
```
1.4 PUT​/api​/v1​/Activities​/{id}<br/>
HTTP-метод - PUT <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Activities/1 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса - 
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-06T21:19:58.019Z",
  "completed": true
}
``` 
Статус-код ответа - 200 <br/>
Тело ответа - 
```
[
  {
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-06T21:19:58.019Z",
  "completed": true
}]
```
1.5 DELETE​/api​/v1​/Activities​/{id}<br/>
HTTP-метод - DELETE <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Activities/1 <br/>
Заголовки запроса - "accept: */*" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - отсутствует <br/>

### Провальные:

1.6 GET/api​/v1​/Activities​/{id} <br/>   
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Activities/0987654321 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 404 <br/>
Тело ответа -
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-2724d78dd2ab6f4baaefb390c3e4074c-17be3b76319f4f4e-00"
}
```
1.7 PUT​/api​/v1​/Activities​/{id}<br/>
HTTP-метод - PUT <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Activities/0987654321  <br/>
Заголовки запроса - "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" <br/>
Статус-код ответа - 400 <br/>
Тело запроса - 
```
{
  "id": 0987654321,
  "title": "string",
  "dueDate": "2023-06-06T21:19:58.019Z",
  "completed": true
}
``` 
Статус-код ответа - 400 <br/>
Тело ответа - 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-db5c98bdf78cf0478edbb4288ad72881-38af2a14df4a4e42-00",
  "errors": {
    "$.id": [
      "Invalid leading zero before '9'. Path: $.id | LineNumber: 1 | BytePositionInLine: 9."
    ]
  }
}
```

1.8 POST​/api​/v1​/Activities<br/>
HTTP-метод - POST <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Activities<br/>
Заголовки запроса - "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса 
```
{{
  "id": 0987654321,
  "title": "string",
  "dueDate": "2023-06-06T21:12:51.288Z",
  "completed": true
}
``` 
Статус-код ответа - 400 <br/>
Тело ответа - 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-0276f45b7dcfef4fbde890340cfea776-06ce5378a0877442-00",
  "errors": {
    "$.id": [
      "Invalid leading zero before '9'. Path: $.id | LineNumber: 1 | BytePositionInLine: 9."
    ]
  }
}
```
## 2. Authors 
### Успешные:    
2.1 GET/api​/v1​/Authors <br/>  
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Authors <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - 
```
[
  {
    "id": 1,
    "idBook": 1,
    "firstName": "First Name 1",
    "lastName": "Last Name 1"
  }
]
```
2.2 POST​/api​/v1​/Authors
HTTP-метод - POST <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Authors <br/>
Заголовки запроса - "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса - 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
``` 
Статус-код ответа - 200 <br/>
Тело ответа - 
```
[
  {
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
]
```

2.3 GET ​/api​/v1​/Authors​/authors​/books​/{idBook}<br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/1 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - 
```
{
    "id": 1,
    "idBook": 1,
    "firstName": "First Name 1",
    "lastName": "Last Name 1"
  }
  ```
2.4 GET​/api​/v1​/Authors​/{id}<br/>  
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Authors/1 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - 
```
{
  "id": 1,
  "idBook": 1,
  "firstName": "First Name 1",
  "lastName": "Last Name 1"
}
```
2.5 PUT​/api​/v1​/Authors​/{id}<br/>
HTTP-метод - PUT <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Authors/1 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса - 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
``` 
Статус-код ответа - 200 <br/>
Тело ответа - 
```
[
  {
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}]
```
2.6 DELETE​/api​/v1​/Authors​/{id}<br/>
HTTP-метод - DELETE <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Authors/1 <br/>
Заголовки запроса - "accept: */*" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - отсутствует <br/>

### Провальные:

2.7 GET/api/v1/Authors/authors/books/{idBook} <br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/70987654321 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 400 <br/>
Тело ответа - 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-56100928cec0e54983252f2c3b5fa997-197c715ab3d52a4c-00",
  "errors": {
    "idBook": [
      "The value '70987654321' is not valid."
    ]
  }
}}
```
2.8 POST​/api​/v1​/Authors <br/>
HTTP-метод - POST <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Authors <br/>
Заголовки запроса - "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса 
```
{
  "id": 70987654321,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
``` 
Статус-код ответа - 400 <br/>
Тело ответа - 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-9a9ea7eee2c5704aa48945d349f57013-ee17f3714265124f-00",
  "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 0 | BytePositionInLine: 17."
    ]
  }
}
```
2.9 GET​//api/v1/Authors/{id}<br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Authors/70987654321 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 400 <br/>
Тело ответа - 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-ae4e153b9a17564da23c12b97553a4d1-e575d31a2e931343-00",
  "errors": {
    "id": [
      "The value '70987654321' is not valid."
    ]
  }
}
```
2.10 PUT​/api​/v1​/Authors​/{id}<br/>
HTTP-метод - PUT <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Authors/70987654321 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса - 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
``` 
Статус-код ответа - 400 <br/>
Тело ответа - 
```
[{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-d17f9e835cc4174781a5abee53bc1fa3-8b2a3bc1238e8a48-00",
  "errors": {
    "id": [
      "The value '70987654321' is not valid."
    ]
  }
}]
```
## 3. Books 
### Успешные:  
3.1 GET/api​/v1​/Books <br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Books <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - 
```
[
  {
    "id": 1,
    "title": "Book 1",
    "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "pageCount": 100,
    "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "publishDate": "2023-06-05T22:14:38.0598665+00:00"
  },
  {
    "id": 2,
    "title": "Book 2",
    "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "pageCount": 200,
    "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "publishDate": "2023-06-04T22:14:38.0598809+00:00"
  }
]
```
3.2 POST​/api​/v1​/Books <br/>
HTTP-метод - POST <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Books <br/>
Заголовки запроса - "accept: */*" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса -
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-06T22:16:11.828Z"
}
``` 
Статус-код ответа - 200 <br/>
Тело ответа - 
```
[
  {
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-06T22:16:11.828Z"
}]
```
3.3 GET ​/api​/v1​/Authors​/authors​/books​/{idBook}<br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Books/1 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - 
```
{
  "id": 1,
  "title": "Book 1",
  "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
  "pageCount": 100,
  "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
  "publishDate": "2023-06-05T22:22:43.7375246+00:00"
}
```
3.4 PUT​/api​/v1​/Books​/{id}<br/>
HTTP-метод - PUT <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Books/1 <br/>
Заголовки запроса - "accept: */*" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса - 
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-06T22:25:05.715Z"
}
``` 
Статус-код ответа - 200 <br/>
Тело ответа - 
```
[
  {
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-06T22:25:05.715Z"
}
```
3.5 DELETE​​/api​/v1​/Books​/{id}<br/>
HTTP-метод - DELETE <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Books/1 <br/>
Заголовки запроса - "accept: */*" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - отсутствует <br/>

### Провальные
3.6 GET/api/v1/Books/{id}<br/> 
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Books/70987654321 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 400 <br/>
Тело ответа - 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-f96903f61744444699b5bb54b84b832b-29365dd4cc644d42-00",
  "errors": {
    "id": [
      "The value '70987654321' is not valid."
    ]
  }
}
```
3.7 POST​/api/v1/Books<br/>
HTTP-метод - POST <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Books <br/>
Заголовки запроса - "accept: */*" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса -
```
{
  "id": 70987654321,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-06T22:16:11.828Z"
}}
``` 
Статус-код ответа - 400 <br/>
Тело ответа - 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-5e3cb0ae23fd5040b7a8a82522f954a8-7ac7068691129f48-00",
  "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 0 | BytePositionInLine: 17."
    ]
  }
}]
```
3.8 PUT​/api/v1/Books/{id}<br/>
HTTP-метод - PUT <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Books/70987654321 <br/>
Заголовки запроса - "accept: */*" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса - 
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-06T22:25:05.715Z"
}
``` 
Статус-код ответа - 400 <br/>
Тело ответа - 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-6e3a75ca1717b849b18233a1d96e8024-36f90ec673fcbd49-00",
  "errors": {
    "id": [
      "The value '70987654321' is not valid."
    ]
  }
}
```
## 4. CoverPhotos 
### Успешные:   
4.1 GET​/api​/v1​/CoverPhotos <br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - 
```
[
  {
    "id": 1,
    "idBook": 1,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"
  },
  {
    "id": 2,
    "idBook": 2,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 2&w=250&h=350"
  }
]
```
4.2 POST​/api​/v1​/CoverPhotos<br/>
HTTP-метод - POST <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos<br/>
Заголовки запроса - "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса - 
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
``` 
Статус-код ответа - 200 <br/>
Тело ответа - 
```[
  {
  "id": 0,
  "idBook": 0,
  "url": "string"
}
]
```
4.3 GET​/api​/v1​/CoverPhotos​/books​/covers​/{idBook}<br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/1 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - 
```
{
    "id": 1,
    "idBook": 1,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"
  }
  ```
4.4 GET​/api​/v1​/CoverPhotos​/{id}  <br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/1 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - 
```
{
  "id": 1,
  "idBook": 1,
  "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"
}
```
4.5 PUT​​/api​/v1​/CoverPhotos​/{id}<br/>
HTTP-метод - PUT <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/1 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса - 
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
``` 
Статус-код ответа - 200 <br/>
Тело ответа - 
```
[
  {
  "id": 0,
  "idBook": 0,
  "url": "string"
}]
```
4.6 DELETE​/api/v1/CoverPhotos/{id}<br/>
HTTP-метод - DELETE <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/1<br/>
Заголовки запроса - "accept: */*"<br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - отсутствует <br/>
### Провальные:
4.7 GET​/api​/v1​/CoverPhotos​/books​/covers​/{idBook}<br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/70987654321 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 400 <br/>
Тело ответа - 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-48b40279fe89cc47a0ee0e1e5672713c-ed60635c2a045b48-00",
  "errors": {
    "idBook": [
      "The value '70987654321' is not valid."
    ]
  }
  ```
4.8 POST​/api​/v1​/CoverPhotos<br/>
HTTP-метод - POST <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos <br/>
Заголовки запроса - "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса -
```
{
  "id": 70987654321
  "idBook": 0,
  "url": "string"
}
``` 
Статус-код ответа - 400 <br/>
Тело ответа - 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-12eaafdb73e69a40a5d1ec8fdd654f16-dd614843ccb7e14a-00",
  "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 19."
    ]
  }
  ```
4.9 GET​/api​/v1​/CoverPhotos​/{id}<br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/70987654321 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 400 <br/>
Тело ответа -
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-02ea360407d08d4b9a4bfd03349cf9b3-6483a2efb5f81349-00",
  "errors": {
    "id": [
      "The value '70987654321' is not valid."
    ]
  }
}
```
4.10 PUT​/api​/v1​/CoverPhotos​/{id}<br/>
HTTP-метод - PUT <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/70987654321 <br/>
Заголовки запроса - "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса - 
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}}
``` 
Статус-код ответа - 400 <br/>
Тело ответа - 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-ab9502d1ee1c064788ba88ef0845d05a-c780dd73e9a3f841-00",
  "errors": {
    "id": [
      "The value '70987654321' is not valid."
    ]
  }
}
```
## 5. Users 

### Успешные:

5.1 GET​/api​/v1​/Users <br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Users <br/>
Заголовки запроса - "accept: text/plain; v=1.0" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - 
```
[
  {
    "id": 1,
    "userName": "User 1",
    "password": "Password1"
  },
  {
    "id": 2,
    "userName": "User 2",
    "password": "Password2"
  },
  {
    "id": 3,
    "userName": "User 3",
    "password": "Password3"
  }
  
]
```
5.2 POST​​/api​/v1​/Users<br/>
HTTP-метод - POST <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Users <br/>
Заголовки запроса - "accept: */*" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса -
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
``` 
Статус-код ответа - 200 <br/>
Тело ответа - 
```
  {
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
5.3 GET​/api​/v1​/Users​/{id}<br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Users/1 <br/>
Заголовки запроса - "accept: */*" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - 
```
{
  "id": 1,
  "userName": "User 1",
  "password": "Password1"
}
```
5.4 PUT​//api/v1/Users/{id}<br/>
HTTP-метод - PUT <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Users/1 <br/>
Заголовки запроса - "accept: */*" -H  "Content-Type: application/json; v=1.0"  <br/>
Тело запроса -
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
``` 
Статус-код ответа - 200 <br/>
Тело ответа - 
``` 
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
5.5 DELETE​​//api/v1/Users/{id}<br/>
HTTP-метод - DELETE <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Users/1 <br/>
Заголовки запроса - "accept: */*" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 200 <br/>
Тело ответа - отсутствует <br/>
### Провальные 
5.6 GET​/api​/v1​/Users​/{id}<br/>
HTTP-метод - GET <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Users/70987654321 <br/>
Заголовки запроса - "accept: */*" <br/>
Тело запроса - отсутствует <br/>
Статус-код ответа - 400 <br/>
Тело ответа - 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-1b33a37e8afe114aa07190924f0bf854-8f85081d4ba9a74d-00",
  "errors": {
    "id": [
      "The value '70987654321' is not valid."
    ]
  }
}
```
5.7 POST​/api​/v1​/Users<br/>
HTTP-метод - POST <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Users <br/>
Заголовки запроса - "accept: */*" -H  "Content-Type: application/json; v=1.0" <br/>
Тело запроса -
```
{
  "id": 70987654321,
  "userName": "string",
  "password": "string"
}}
``` 
Статус-код ответа - 400 <br/>
Тело ответа - 
```
  {
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-2d43c1adf1a63247ba6a3ece2dd3ab3e-5462f4074a26b049-00",
  "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 0 | BytePositionInLine: 17."
    ]
  }
}
```
5.8 PUT​/api​/v1​/Users​/{id}<br/>
HTTP-метод - PUT <br/>
Полный URL запроса - https://fakerestapi.azurewebsites.net/api/v1/Users/70987654321 <br/>
Заголовки запроса - "accept: */*" -H  "Content-Type: application/json; v=1.0"  <br/>
Тело запроса - 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
``` 
Статус-код ответа - 400 <br/>
Тело ответа - 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-2359c187c5fcac468ef3502328aa6a99-a57af91e1cfb3c43-00",
  "errors": {
    "id": [
      "The value '70987654321' is not valid."
    ]
  }
  ```
